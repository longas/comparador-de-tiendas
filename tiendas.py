from bs4 import BeautifulSoup as bs
import urllib2

#Direcciones de los productos
pccom = "http://www.pccomponentes.com/razer_deathadder_2013_6400_dpi.html"
pcbox = "http://www.pcbox.com/comprar-raton-razer-deathadder-2013-gaming-negro-6400dpi_raz15.aspx?ch=0000020511000703051203120802127edfbe36301643c7816e4d64208a30a60#.UoOymPnkmVw"
frags = "http://www.4frags.com/catalog/product_info.php?products_id=62429&name=raton+razer+deathadder+4g+-+6400dpi+-+versi%F3n+2013"

#Abrimos la web
pccom = urllib2.urlopen(pccom)
pcbox = urllib2.urlopen(pcbox)
frags = urllib2.urlopen(frags)

#Usamos BeautifulSoup
pccom = bs(pccom)
pcbox = bs(pcbox)
frags = bs(frags)

#Sacamos los precios del HTML
pccom = pccom.find('meta', {'itemprop': 'price'})
pccom = pccom.get('content')
pcbox = pcbox.find('div', {'class': 'fichaPrecioPW'})
pcbox = pcbox.get_text().strip()[:-1]
frags = frags.find('div', {'class': 'producto-precio'})
frags = frags.get_text().strip()[:-1]

#Creamos un diccionario con los nombres y precios
precios = {
	'PC Componentes' : pccom,
	'PCBOX' : pcbox,
	'4Frags' : frags
}

#Conseguimos el mas barato
barato_nombre = ""
barato_precio = min(pccom, pcbox, frags)

for nombre, precio in precios.items():
	if precio == barato_precio:
		barato_nombre = nombre

#Sacamos por pantalla los precios
print "----------------------------------"
print "Precios del raton Razer Deathadder"
print "----------------------------------"
for nombre, precio in precios.items():
	print nombre + ":", precio + u"\u20AC"
print "----------------------------------"
print "El mas barato es:", barato_precio + u"\u20AC", "en", barato_nombre
print "----------------------------------"